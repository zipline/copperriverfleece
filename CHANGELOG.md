# *Milano BigCommerce Stencil Theme Changelog*

## Sep 06, 2019 v1.0.4
- Bug:  Default Product Thumbnail will not show on the product grid 

## Feb 13, 2019 v1.0.3
- Bug: Homepage Recent Blog limit issue with changing style
- Bug: Sticky header issue in Blue Style
- Bug: Footer Newsletter subscriber form submission issue
- Enhancement: Footer social media icons changed with SVG image
- Enhancement: Upgraded theme with latest features of cornerstone (Enhance eCommerce tracking, Saved Payment methods)
- Enhancement: Cookies base pencil banner
- Enhancement: Out of stock message style on products page

## Oct 26, 2018 v1.0.2
- Bug: Fixed issue with Wishlist count not showing on header in some pages
- Bug: Product page - Login for pricing text still appearing in the related product section even user already logged in.
- Bug: Optimized checkout order confirmation page logo not showing
- Enhancement: Added option in Theme Editor for footer text color
- Enhancement: Added option in Theme Editor for footer social media icon color

## Oct 10, 2018 v1.0.1
- Theme Launched
